$("#submit").click(function(event) {
    let dic = {
        r0: "00",
        r1: "01",
        r2: "10",
        r3: "11"
    }
    let resultArr = []
    let str = $("#assembly").val();
    let lines = str.split("\n");
    let list = [];
    for (let i = 0; i < lines.length; i++) {
        list.push(lines[i].toLowerCase().split(/[\s,]+/))
    }

    for (let i = 0; i < list.length; i++) {
        switch (list[i][0]) {
            case "load":
                result = "00" + dic[list[i][1]] + "01" + " -- value: " + list[i][2];
                html = "<h3>" + result + "</h3>"
                break;
            case "add":
                result = "01" + dic[list[i][1]] + dic[list[i][2]];
                html = "<h3>" + result + "</h3>"
                break;
            case "sub":
                result = "10" + dic[list[i][1]] + dic[list[i][2]];
                html = "<h3>" + result + "</h3>"
                break;
            case "jmp":
                result = "00" + dic[list[i][1]] + "10" + " -- address: " + list[i][2];
                html = "<h3>" + result + "</h3>"
                break;
        }
        resultArr.push(html);
    }
    // console.log(resultArr);
    $("#resultDiv").html(resultArr)
    
  });